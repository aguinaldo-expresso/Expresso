/**
 * Tine 2.0
 * 
 * @package     AppLauncher
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Rommel Cysne <rommel.cysne@serpro.gov.br>
 * @copyright   Copyright (c) 2007-2011 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */

Ext.ns('Tine.AppLauncher');

/**
 * @namespace   Tine.AppLauncher
 * @class       Tine.AppLauncher.Application
 * @extends     Tine.Tinebase.Application
 * AppLauncher Application Object <br>
 * 
 * @author      Rommel Cysne <rommel.cysne@serpro.gov.br>
 */
Tine.AppLauncher.Application = Ext.extend(Tine.Tinebase.Application, {

});

/**
 * Component for the iframe component of the webconference application.
 *
 */
//Ext.ux.IFrameComponent = Ext.extend(Ext.BoxComponent, {
//    onRender : function(ct, position){
//	this.el = ct.createChild({
//	    tag: 'iframe', 
//	    id: 'webconference-iframe', 
//	    name: 'webconference-iframe',
//	    frameBorder: 0
////	    src: this.url
//	});
//    }
//});

/**
 * @namespace Tine.AppLauncher
 * @class Tine.AppLauncher.MainScreen
 * @extends Tine.widgets.MainScreen
 * MainScreen of the AppLauncher Application <br>
 * 
 * 
 * @constructor
 * Constructs mainscreen of the AppLauncher application
 */

Tine.AppLauncher.MainScreen = function(config) {
    activeContentType: 'AppLauncher'
    Ext.apply(this, config);

    this.initPanels();
    this.initActions();

    Tine.AppLauncher.MainScreen.superclass.constructor.apply(this, arguments);

};

Ext.extend(Tine.AppLauncher.MainScreen, Tine.widgets.MainScreen, {

    mainScreenPanel: null,
    
    initActions: function() {
    },

    initPanels: function(){
        this.app = Tine.Tinebase.appMgr.get('AppLauncher');
        this.mainScreenPanel = new Tine.AppLauncher.MainScreenCenterPanel();
    },

    getNorthPanel: function(contentType) {

	contentType = contentType || this.getActiveContentType();

        if (! this[contentType + 'ActionToolbar']) {
            try {

                this[contentType + 'ActionToolbar'] = new Ext.Toolbar({
                    items: {
                        height: 56
                    }
                });

            } catch (e) {
                Tine.log.err('Could not create northPanel');
                Tine.log.err(e);
                this[contentType + 'ActionToolbar'] = new Ext.Panel({
                    html: 'ERROR'
                });
            }
        }

	return this[contentType + 'ActionToolbar'];
    },

    getCenterPanel: function() {
	return this.mainScreenPanel;
    },

    getWestPanel: function() {
        var contentType = this.getActiveContentType(),
            wpName = contentType + 'WestPanel';
            
	if (! this.westPanel) {

	    this.westPanel = new Tine.widgets.mainscreen.WestPanel({
		app: Tine.Tinebase.appMgr.get('AppLauncher'),
		hasFavoritesPanel: false,
		hasContentTypeTreePanel:false,
		hasContainerTreePanel:false,
                layout: 'fit',
		additionalItems: [ new Ext.FormPanel({
		    frame: true,
//		    title: this.app.i18n._('Processo Decisório'),
		    labelAlign	: 'top',
                    
                    items: [
		    {
//                        height: 650
		    }
		    ]
		}) ]
	    });
	}

	return this.westPanel;
    },

    show: function() {
	if(this.fireEvent("beforeshow", this) !== false){
	    this.showWestPanel();
	    this.showCenterPanel();
	    this.showNorthPanel();
            this.showModuleTreePanel();

	    this.fireEvent('show', this);
	}

	return this;
    }

});