Tine.Calendar.Printer.MonthViewRenderer = Ext.extend(Tine.Calendar.Printer.BaseRenderer, {
    paperHeight: 600,
    
    generateBody: function(view) {
        var daysHtml = this.splitDays(view.store, view.dateMesh, view.dateMesh.length),
            body = [];
        
        body.push(this.getPageSize());
        
        // title
        body.push(this.generateTitle(view));
        
        workdays=0;
        y=1;
        for(var j=0;j < 7 ; j++){
            g = view.work & y;
            if(g > 0) {
                workdays++;
            }
            y <<= 1;
        }

        // day headers
        var dayNames = [];
        y=1;
        for(var i = 0; i < 7; i++){
            var d = (view.startDay+i)%7;
            g = view.work & y;
            if(g > 0) {
                dayNames.push("<td class='cal-print-monthview-daycell'><span>", view.dayNames[d], "</span></td>");
            }
            y <<= 1;
        }

        //body
        body.push(String.format('<table class="cal-print-monthview"><tr>{0}</thead>{1}</tr>', dayNames.join("\n"), this.generateCalRows(daysHtml, workdays, true)));
        return body.join("\n");
    },
    
    getTitle: function(view) {
        return view.dateMesh[10].format('F Y');
    },
    
    dayHeadersTpl: new Ext.XTemplate(
        '<tr>',
            '<tpl for=".">',
                '<th>\{{dataIndex}\}</th>',
            '</tpl>',
        '</tr>'
    )
});
