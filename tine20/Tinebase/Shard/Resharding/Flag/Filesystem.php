<?php
/**
 * Filesystem backend class for Resharding Flag
 *
 * @package     Shard
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 * @copyright   Copyright (c); 2015 Metaways Infosystems GmbH (http://www.metaways.de);
 *
 */

/**
 * Filesystem backend class for Resharding Flag
 * @package     Shard
 */
class Tinebase_Shard_Resharding_Flag_Filesystem implements Tinebase_Shard_Resharding_Flag_Interface
{
   /**
    * Full path of file where content of this backend is persisted
    *
    * @var string
    */
    private $_fileName;

    /**
     * holds the instance of the singleton
     *
     * @var Tinebase_Shard_Manager
     */
    private static $instance = NULL;

    /**
     * the constructor
     *
     * don't use the constructor. use the singleton
     */
    private function __construct() {}

    /**
     * don't clone. Use the singleton.
     */
    private function __clone() {}

    /**
     * the singleton pattern
     *
     * @param array $_options
     * @param string $_database
     * @return Tinebase_Shard_Resharding_Flag_Filesystem
     */
    public static function getInstance(array $_options, $_database)
    {
        if (self::$instance === NULL) {
            self::$instance = new Tinebase_Shard_Resharding_Flag_Filesystem();
            self::$instance->_fileName = $_options['filePath'] . DIRECTORY_SEPARATOR
                                       . 'reshardingflag.' . $_database
                                       . '.' . Tinebase_Config::getDomain();
            if (!file_exists(self::$instance->_fileName)) {
                $result = file_put_contents(self::$instance->_fileName, 'disabled', LOCK_EX);

                if ($result === FALSE) {
                    throw new Exception('Can not create resharding flag file: ' . $this->_fileName);
                }
            }
        }
        return self::$instance;
    }

    /**
     * get shardkey that is in resharding process or 'disabled'
     *
     * @return string $_shardkey | string "disabled"
     */
    public function getResharding()
    {
        $result = file_get_contents($this->_fileName);

        if ($result === FALSE) {
            throw new Exception('Can not read from resharding flag file: ' . $this->_fileName);
        }

        return $result;
    }

    /**
     * Set resharding shardkey or 'disabled'
     *
     * @param string $_shardKey | NULL
     */
    public function setResharding($_shardKey = NULL)
    {
        if (!isset($_shardKey)) {
            $_shardKey = 'disabled';
        }
        $result = file_put_contents($this->_fileName, $_shardKey, LOCK_EX);

        if ($result === FALSE) {
            throw new Exception('Can not write to resharding flag file: ' . $this->_fileName);
        }

        return $result;
    }
}