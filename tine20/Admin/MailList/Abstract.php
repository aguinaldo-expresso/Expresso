<?php
/**
 * Tine 2.0
 *
 * @package     Admin
 * @subpackage  MailList
 * @license     http://www.gnu.org/licenses/agpl.html AGPL3
 * @copyright   Copyright (c) 2014 SERPRO (https://www.serpro.gov.br)
 * @author      Fernando Wendt <fernando-alberto.wendt@serpro.gov.br>
 *
 */

/**
 * abstract class for all mail list backends
 *
 * @package     Admin
 * @subpackage  MailList
 */

abstract class Admin_MailList_Abstract
{
    /**
     * return all mail list an account is member of
     *
     * @param mixed $_accountId the account as integer or Tinebase_Model_User
     * @return array
     */
    abstract public function getMailListMemberships($_accountId);

    /**
     * get list of mail list members
     *
     * @param int $_mailListId
     * @return array
     */
    abstract public function getMailListMembers($_mailListId);

    /**
     * replace all current mail list members with the new mail list members
     *
     * @param int $_mailListId
     * @param array $_listMembers
     * @return unknown
     */
    abstract public function setMailListMembers($_mailListId, $_listMembers);

    /**
     * add a new mail list member to the list
     *
     * @param int $_mailListId
     * @param int $_accountId
     * @return unknown
     */
    abstract public function addMailListMember($_mailListId, $_accountId);

    /**
     * remove one mail list member from the list
     *
     * @param int $_groupId
     * @param int $_accountId
     * @return unknown
     */
    abstract public function removeMailListMember($_mailListId, $_accountId);

    /**
     * create a new mail list
     *
     * @param string $_mailList
     * @return unknown
     */
    abstract public function addMailList(Admin_Model_MailList $_mailList);

    /**
     * updates an existing mail list
     *
     * @param Admin_Model_MailList $_mailList
     * @return Admin_Model_MailList
     */
    abstract public function updateMailList(Admin_Model_MailList $_mailList);

    /**
     * remove mail list
     *
     * @param mixed $_mailListId
     *
     */
    abstract public function deleteMailList($_mailListId);

    /**
     * get mail list by id
     *
     * @param int $_mailListId
     * @return Admin_Model_MailList
     * @throws  Tinebase_Exception_Record_NotDefined
     */
    abstract public function getMailListById($_mailListId);

    /**
     * get mail list by name
     *
     * @param string $_mailListName
     * @return Admin_Model_MailList
     * @throws  Tinebase_Exception_Record_NotDefined
     */
    abstract public function getMailListByName($_mailListName);

    /**
     * get mail list by admin name
     *
     * @param array $_mailListAdmin
     * @return Admin_Model_MailList
     * @throws  Tinebase_Exception_Record_NotDefined
     */
    abstract public function getMailListByAdmin($_mailListAdmin);

    /**
     * get mail list by type
     *
     * @param string $_mailListType
     * @return Admin_Model_MailList
     * @throws  Tinebase_Exception_Record_NotDefined
     */
    abstract public function getMailListByType($_mailListType);

    /**
     * get mail list administrators
     *
     * @param int $_mailListId
     * @return Admin_Model_MailList
     * @throws  Tinebase_Exception_Record_NotDefined
     */
    abstract public function getMailListAdmin($_mailListId);

    /**
     * import mail list from file
     *
     * @param file $_mailListData
     * @return Admin_Model_MailList
     * @throws  Tinebase_Exception_Record_NotDefined
     */
    abstract public function importMailList($_mailListData);

    /**
     * export mail list to file
     *
     * @param file $_mailListId
     * @return Admin_Model_MailList
     * @throws  Tinebase_Exception_Record_NotDefined
     */
    abstract public function exportMailList($_mailListId);

    /**
     * get list of mail list (searching result)
     *
     * @param string $_filter
     * @param string $_sort
     * @param string $_dir
     * @param int $_start
     * @param int $_limit
     * @return Tinebase_Record_RecordSet with record class Tinebase_Model_Group
     */
    abstract public function search($_filter = NULL, $_paging = NULL);

    /**
    * get dummy mail list record
    *
    * @param integer $_id [optional]
    * @return Tinebase_Model_Group
    */
    public function getNonExistentMailList($_id = NULL)
    {
        $translate = Tinebase_Translation::getTranslation('Tinebase');

        $result = new Admin_Model_MailList(array(
                'id'        => $_id,
                'name'      => $translate->_('unknown'),
        ), TRUE);

        return $result;
    }
}