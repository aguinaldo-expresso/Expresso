<?php

class Webconference_Backend_BigBlueButtonApi 
{
    
  private function requestCURL($_url, $_parameters = NULL, $_method = 'GET'){
	if (extension_loaded('curl')) {
	    $ch = curl_init() or die ( curl_error() );
	    $timeout = 10;
	    curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, $_method);
	    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt( $ch, CURLOPT_URL, $_url );
	    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
	    curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	    if ($_method === 'POST'){
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
		curl_setopt( $ch, CURLOPT_POST, true);
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $_parameters);
	    }

	    $data = curl_exec( $ch );

	    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    $result = NULL;
	    if($httpCode == 200) {
		$result = new SimpleXMLElement($data);
	    }
	    curl_close( $ch );
	    return $result;
	}
	return simplexml_load_file($_url);
    }


    private function bbb_wrap_json($_url) 
    {
	return json_encode($this->requestCURL($_url));
    }

    private function getMeetingsURL($_URL, $_SALT) 
    {
	$base_url = $_URL . "api/getMeetings?";
	$params = 'random=' . (rand() * 1000 );
	return ( $base_url . $params . '&checksum=' . sha1('getMeetings' . $params . $_SALT));
    }

    private function getMeetingInfoURL($_meetingID, $_modPW, $_URL, $_SALT) 
    {
	$base_url = $_URL . "api/getMeetingInfo?";
	$params = 'meetingID=' . urlencode($_meetingID) . '&password=' . urlencode($_modPW);
	return ( $base_url . $params . '&checksum=' . sha1("getMeetingInfo" . $params . $_SALT));
    }

    private function createMeetingURL($_name, $_meetingID, $_attendeePW, $_moderatorPW, $_welcome, $_logoutURL, $_SALT, $_URL, $_meta = NULL)
    {
	$url_create = $_URL . "api/create?";
	$voiceBridge = 70000 + rand(0, 9999);
	$params = 'name=' . urlencode($_name) . '&meetingID=' . urlencode($_meetingID) . '&attendeePW=' . urlencode($_attendeePW) . '&moderatorPW=' . urlencode($_moderatorPW) . '&voiceBridge=' . $voiceBridge . '&logoutURL=' . urlencode($_logoutURL);
	if (trim($_welcome)){
	    $params .= '&welcome=' . urlencode($_welcome);
	}

	if ($_meta != NULL){
	    $meta = '';
	    foreach ($_meta as $key => $value) {
		$meta .= '&meta_' . urlencode($key) . '=' . urlencode($value);
	    }
	    $params .= $meta;
	}

	return ( $url_create . $params . '&checksum=' . sha1("create" . $params . $_SALT) );
    }

    private function endMeetingURL($_meetingID, $_modPW, $_URL, $_SALT) 
    {
	$base_url = $_URL . "api/end?";
	$params = 'meetingID=' . urlencode($_meetingID) . '&password=' . urlencode($_modPW);
	return ( $base_url . $params . '&checksum=' . sha1("end" . $params . $_SALT) );
    }

    public function joinURL($_meetingID, $_userName, $_PW, $_SALT, $_URL, $_configTokenXML = NULL)
    {
	$url_join = $_URL . "api/join?";
	$params = 'meetingID=' . urlencode($_meetingID) . '&fullName=' . urlencode($_userName) . '&password=' . urlencode($_PW);
	if ($_configTokenXML != NULL){
	    $params = $params . '&configToken=' . urlencode($_configTokenXML);
	}
	return ($url_join . $params . '&checksum=' . sha1("join" . $params . $_SALT) );
    }

    public function getMeetings($_URL, $_SALT) 
    {
	$result = json_decode($this->bbb_wrap_json($this->getMeetingsURL($_URL, $_SALT)));
	if ($result && $result->returncode == 'SUCCESS' && $result->messageKey) {
	    return (object) array('returncode' => $result->returncode, 'message' => $result->message, 'messageKey' => $result->messageKey);
	} else if ($result && $result->returncode == 'SUCCESS') { //If there were meetings already created
	    foreach ($result->meetings as $meeting) {
		$meetings[] = array('meetingID' => $meeting->meetingID, 'moderatorPW' => $meeting->moderatorPW, 'attendeePW' => $meeting->attendeePW, 'hasBeenForciblyEnded' => $meeting->hasBeenForciblyEnded, 'running' => $meeting->running);
	    }
	    return (object) $meetings;
	} else if ($result) { //If the xml packet returned failure it displays the message to the user
	    return (object) array('returncode' => $result->returncode, 'message' => $result->message, 'messageKey' => $result->messageKey);
	} else { //If the server is unreachable, then prompts the user of the necessary action
	    return null;
	}
    }

    public function endMeeting($_meetingID, $_modPW, $_URL, $_SALT) 
    {
	$result = json_decode($this->bbb_wrap_json($this->endMeetingURL($_meetingID, $_modPW, $_URL, $_SALT)));
	if ($result) {
	    return (object) array('returncode' => $result->returncode, 'message' => $result->message, 'messageKey' => $result->messageKey);
	} else {
	    return null;
	}
    }

    public function getUsers($_meetingID, $_modPW, $_URL, $_SALT) 
    {
	$result = json_decode($this->bbb_wrap_json($this->getMeetingInfoURL($_meetingID, $_modPW, $_URL, $_SALT)));
	if ($result && $result->returncode == 'SUCCESS' && $result->messageKey == null) {
	    return (object) array('returncode' => $result->returncode, 'message' => $result->message, 'messageKey' => $result->messageKey);
	} else if ($result && $result->returncode == 'SUCCESS') {
	    foreach ($result->attendees->attendee as $attendee) {
		$users[] = array('userID' => $attendee->userID, 'fullName' => $attendee->fullName, 'role' => $attendee->role);
	    }
	    return (object) $users;
	} else if ($result) {
	    return (object) array('returncode' => $result->returncode, 'message' => $result->message, 'messageKey' => $result->messageKey);
	} else {
	    return null;
	}
    }

    public function getMeetingIsActive($_meetingID, $_modPW, $_URL, $_SALT) 
    {
	$result = json_decode($this->bbb_wrap_json($this->getMeetingInfoURL($_meetingID, $_modPW, $_URL, $_SALT)));
	if ($result && $result->returncode == 'SUCCESS' && !$result->messageKey == null && $result->hasBeenForciblyEnded == 'false') {//The meetings were returned
	    return true;
	} else {
	    return false;
	}
    }

    public function createMeeting($_title, $_meetingID, $_welcomeString, $_mPW, $_aPW, $_SALT, $_URL, $_logoutURL, $_meta = NULL)
    {
	$result = json_decode($this->bbb_wrap_json($this->createMeetingURL($_title, $_meetingID, $_aPW, $_mPW, $_welcomeString, $_logoutURL, $_SALT, $_URL, $_meta)));
	if ($result) {
	    if ($result->meetingID)
		return (object) array('returncode' => $result->returncode, 'message' => $result->message, 'messageKey' => $result->messageKey, 'meetingID' => $result->meetingID, 'attendeePW' => $result->attendeePW, 'moderatorPW' => $result->moderatorPW, 'hasBeenForciblyEnded' => $result->hasBeenForciblyEnded);
	    else
		return (object) array('returncode' => $result->returncode, 'message' => $result->message, 'messageKey' => $result->messageKey);
	}
	else {
	    return null;
	}
    }

    public function getDefaultConfigXML($_SALT, $_URL)
    {
	$base_url = $_URL . "api/getDefaultConfigXML.xml?";
	$params =  'checksum=' . sha1("getDefaultConfigXML" . $_SALT);
	$url = $base_url . $params;
	return $this->requestCURL($url);
    }

    public function setConfigXML($_meetingID, $_SALT, $_URL, $_XML)
    {
	$xml = $_XML->asXML();
	/*
	$xml = str_replace(array("\n","\r"),'',$xml);
	$xml = str_replace(array("\t"),' ',$xml);
	$xml = preg_replace('/\s+/',' ',$xml);
	*/
	$url = $_URL ."api/setConfigXML.xml";
	$params = "configXML=".urlencode($xml). "&meetingID=".urlencode($_meetingID);
	$parameters = $params . "&checksum=" . sha1("setConfigXML" . $params . $_SALT);
	$result = $this->requestCURL($url, $parameters, 'POST');
	return ($result);
    }
}