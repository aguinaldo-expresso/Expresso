/*
 * TODO: Licensing
 */
package br.gov.serpro.applet;

import br.gov.serpro.cert.DigitalCertificate;
import br.gov.serpro.cert.WrongPasswordException;
import br.gov.serpro.mail.SMIMEMailGenerator;
import br.gov.serpro.setup.Setup;
import java.applet.Applet;
import java.awt.Frame;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import javax.net.ssl.*;
import javax.swing.JApplet;
import javax.swing.SwingUtilities;
import netscape.javascript.JSException;
import netscape.javascript.JSObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.params.CoreProtocolPNames;
import org.bouncycastle.mail.smime.SMIMEException;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;

//TODO: Logs de depuração e interface para usuário reportar problemas.
public class SmimeApplet extends JApplet {

    /**
     * Variáveis de instância
     */
    private Setup setup;
    private Frame parentFrame;
    private SmimeApplet applet = this;
    private FileDownloader downloader;
    /**
     * Variáveis de classe
     */
    private static final long serialVersionUID = 4797603392324194391L;

    @Override
    public void init() {
        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    setSize(0, 0);
//                    JPanel panel = new JPanel(new BorderLayout());
//                    panel.add(new JLabel("Testando esta josca!"), BorderLayout.CENTER);
//                    
//                    getContentPane().add(panel);
                }
            });
            
        } catch (Exception e) {
            System.err.println("createGUI didn't complete successfully");
        }

        // get params on applet initialization: 
        //  - user agent, componentID
        
        this.setup = new Setup(this);
        this.setup.addLanguageResource("ExpressoCertMailMessages");
        
        this.parentFrame = (Frame) SwingUtilities.getAncestorOfClass(Frame.class, this);
        
        // Init Downloader
    }

    @Override
    /**
     * Retorna Informações sobre os parâmetros que essa applet aceita
     *
     * @author Mário César Kolling <mario.kolling@serpro.gov.br>
     * @return String[][] Uma matriz de Strings relacionando cada parâmetro e
     * sua descrição
     */
    public String[][] getParameterInfo() {
        return setup.getParameterInfo();
    }

    /**
     * Método da Applet chamado pela página (js) ao assinar ou decifrar um
     * e-mail
     *
     * @param resultado Dados serializados passados pela Applet
     */
    public void doButtonClickAction() {
        System.out.println("click");
    }

    public void doButtonClickAction(final String operation, final String id, final String body) {
        Thread t = new Thread(new SmimeRunnable(operation, id, body));
        System.out.println("operation: "+operation);
        System.out.println("id: "+id);
        System.out.println("body: "+body);
        t.start();
        
    }
    
    public FileDownloader getDownloader(String userAgent) throws KeyManagementException, NoSuchAlgorithmException {
        if (this.downloader == null) {
            this.downloader = new FileDownloader(userAgent);
        }
        
        return this.downloader;
    }

    @Override
    public void destroy() {
        // shutdown Downloader and other resources
        if (this.downloader != null) {
            this.downloader.shutdown();
        }
        super.destroy();
    }

    /**
     * @todo Implements parallel downloads
     */
    public class FileDownloader {
        
        private final Applet outerClass = applet;
        private final AbstractHttpClient client;
        
        private FileDownloader(String userAgent) throws NoSuchAlgorithmException, KeyManagementException {
            
            this.client = new DefaultHttpClient();
            this.client.getParams().setParameter(CoreProtocolPNames.USER_AGENT, userAgent);
            URL documentBase = this.outerClass.getDocumentBase();
            
            // permissive Trust Manager
            X509TrustManager tm = new X509TrustManager() {

                @Override
                public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };
            X509HostnameVerifier verifier = new X509HostnameVerifier() {

                @Override
                public void verify(String string, SSLSocket ssls) throws IOException {
                }

                @Override
                public void verify(String string, X509Certificate xc) throws SSLException {
                }

                @Override
                public void verify(String string, String[] strings, String[] strings1) throws SSLException {
                }

                @Override
                public boolean verify(String string, SSLSession ssls) {
                    return true;
                }
            };
            
            if (documentBase.getProtocol().equalsIgnoreCase("https")) {
                SSLContext ctx = SSLContext.getInstance("TLS");
                ctx.init(null, new TrustManager[]{tm}, null);
                SSLSocketFactory ssf = new SSLSocketFactory(ctx, verifier);
                ClientConnectionManager ccm = this.client.getConnectionManager();
                SchemeRegistry sr = ccm.getSchemeRegistry();
                int port = documentBase.getPort();
                sr.register(new Scheme("https", port == -1 ? 443 : port , ssf));
            }
            
        }
        
        public MimeBodyPart downloadFile(URI url, String sessionId) throws ClientProtocolException, IOException {
            System.out.println("arquivo: " + url.toString());
            
            String sessionCookieName = "TINE20SESSID";
            CookieStore cookieStore = this.client.getCookieStore();
            BasicClientCookie tine20SessId = new BasicClientCookie(sessionCookieName, sessionId);
            tine20SessId.setDomain(url.getHost());
            tine20SessId.setPath("/");
            cookieStore.addCookie(tine20SessId);
            
            // Configura um Post request
            HttpGet get = new HttpGet(url);
            
            System.out.println("Executou o request");
            HttpResponse response = this.client.execute(get);
            
            System.out.println("Getting entity");
            HttpEntity httpEntity = response.getEntity();
            
            String contentType = httpEntity.getContentType().getValue();
            //OutputStream content = this._writeToOS(httpEntity.getContent());
            MimeBodyPart part = new MimeBodyPart();
            
            try {
                
                System.out.println("Setting data on MimeBodyPart");
                Logger.getLogger(SMIMEMailGenerator.class.getName()).log(Level.INFO, "contentType: {0}", contentType);
                part.setDataHandler(new DataHandler(new ByteArrayDataSource(httpEntity.getContent(), contentType)));
                part.setHeader("Content-Type", contentType);
                part.setHeader("Content-Transfer-Encoding", "base64");
                
            } catch (Exception e) {
                Logger.getLogger(SMIMEMailGenerator.class.getName()).log(Level.SEVERE, null, e);
            }
            
            return part;
        }
        
        void shutdown(){
            if (this.client != null) {
                this.client.getConnectionManager().shutdown();
            }
        }
        
//        private OutputStream _writeToOS(InputStream input) throws IOException {
//            
//            OutputStream output = new ByteArrayOutputStream();
//            byte[] buffer = new byte[4096];
//            while (input.read(buffer) > 0) {
//                output.write(buffer);
//            }
//            
//            buffer = null;
//            return output;
//        }
        
        
    }
    
    
    private class SmimeRunnable implements Runnable {

        private final String operation;
        private final String id;
        private final String body;
        
        SmimeRunnable(String operation, String id, String body) {
            this.operation = operation;
            this.id = id;
            this.body = body;
        }

        @Override
        public void run() {
            try {
                AccessController.doPrivileged(new SmimePrivilegedAction(operation, id, body));
            } catch (Throwable ex) {
                Throwable e = ex.getCause();
                if (e != null) {
                    System.out.println("problema: " + e.getClass().getName() + 
                            " - " + e.getMessage());
                    e.getMessage();
                }
                Logger.getLogger(SmimeApplet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private class SmimePrivilegedAction implements PrivilegedAction<Object>
    {
        
        private final String userAgent,
                       id,
                       body;
        private final SmimeApplet outerApplet;

        SmimePrivilegedAction(String userAgent, String id, String body) {
            this.userAgent = userAgent;
            this.id = id;
            this.body = body;
            this.outerApplet = applet;
        }
        
        private String _generateJsonResponse(boolean success, String result) {
            
            StringWriter writer = new StringWriter();
            JsonFactory factory = new JsonFactory();

            try {
                JsonGenerator gen = factory.createJsonGenerator(writer);
            
                // Initialize Json
                gen.writeStartObject();
                gen.writeBooleanField("success", success);
                gen.writeStringField("id", this.id);
                gen.writeStringField("msg", result);
                gen.writeEndObject();
                gen.flush();
            
            } catch (IOException ex) {
                Logger.getLogger(SmimeApplet.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            return writer.toString();
        }
        
        @Override
        public Object run() {
            
            DigitalCertificate dc = new DigitalCertificate(parentFrame, setup);
            try {

                // Isto é gambiarra, refatorar parâmetros da ação doButtonClickAction()
                ObjectMapper mapper = new ObjectMapper(new JsonFactory());
                Map<String, Object> message = mapper.readValue(this.body, Map.class);
                dc.init();
                
                // todo: create mail model and refactor this call to use the model
                SMIMEMailGenerator generator = new SMIMEMailGenerator(this.outerApplet, dc, message, this.userAgent);
                MimeMessage completeMessage = generator.generateSignedMail();
                
                if (completeMessage == null){
                    JSObject.getWindow(this.outerApplet).call("appletStub", new String[]{this._generateJsonResponse(true, null)});
                } else {
                    OutputStream result = new ByteArrayOutputStream();
                    completeMessage.writeTo(result);
                    
                    //page.call("Ext.get('"+ parsedData.get("ID") +"').fromApplet", new String[]{"json"});
                    JSObject.getWindow(this.outerApplet).call("appletStub", new String[]{this._generateJsonResponse(true, result.toString())});
                }

            } catch (WrongPasswordException ex) {
                String jsonResponse = this._generateJsonResponse(false, ex.getMessage());
                System.out.println("jsonResponse: " + jsonResponse);
                JSObject.getWindow(this.outerApplet).call("appletStub", new String[]{jsonResponse});
            } catch (GeneralSecurityException ex) {
                OutputStream out = new ByteArrayOutputStream();
                PrintWriter prOut = new PrintWriter(out);
                Logger.getLogger(SmimeApplet.class.getName()).log(Level.SEVERE, null, ex);
                prOut.append(ex.getMessage());
                ex.printStackTrace(prOut);
                prOut.flush();
                JSObject.getWindow(this.outerApplet).call("appletStub", new String[]{this._generateJsonResponse(false, out.toString())});
            } catch (SMIMEException ex) {
                OutputStream out = new ByteArrayOutputStream();
                PrintWriter prOut = new PrintWriter(out);
                Logger.getLogger(SmimeApplet.class.getName()).log(Level.SEVERE, null, ex);
                prOut.append(ex.getMessage());
                ex.printStackTrace(prOut);
                prOut.flush();
                JSObject.getWindow(this.outerApplet).call("appletStub", new String[]{this._generateJsonResponse(false, out.toString())});
            } catch (URISyntaxException ex) {
                OutputStream out = new ByteArrayOutputStream();
                PrintWriter prOut = new PrintWriter(out);
                Logger.getLogger(SmimeApplet.class.getName()).log(Level.SEVERE, null, ex);
                prOut.append(ex.getMessage());
                ex.printStackTrace(prOut);
                prOut.flush();
                JSObject.getWindow(this.outerApplet).call("appletStub", new String[]{this._generateJsonResponse(false, out.toString())});
            } catch (UnsupportedEncodingException ex) {
                OutputStream out = new ByteArrayOutputStream();
                PrintWriter prOut = new PrintWriter(out);
                Logger.getLogger(SmimeApplet.class.getName()).log(Level.SEVERE, null, ex);
                prOut.append(ex.getMessage());
                ex.printStackTrace(prOut);
                prOut.flush();
                JSObject.getWindow(this.outerApplet).call("appletStub", new String[]{this._generateJsonResponse(false, out.toString())});
            } catch (IOException ex) {
                OutputStream out = new ByteArrayOutputStream();
                PrintWriter prOut = new PrintWriter(out);
                Logger.getLogger(SmimeApplet.class.getName()).log(Level.SEVERE, null, ex);
                prOut.append(ex.getMessage());
                ex.printStackTrace(prOut);
                prOut.flush();
                JSObject.getWindow(this.outerApplet).call("appletStub", new String[]{this._generateJsonResponse(false, out.toString())});
            } catch (MessagingException ex) {
                OutputStream out = new ByteArrayOutputStream();
                PrintWriter prOut = new PrintWriter(out);
                Logger.getLogger(SmimeApplet.class.getName()).log(Level.SEVERE, null, ex);
                prOut.append(ex.getMessage());
                ex.printStackTrace(prOut);
                prOut.flush();
                JSObject.getWindow(this.outerApplet).call("appletStub", new String[]{this._generateJsonResponse(false, out.toString())});
            } catch (JSException ex) {
                Logger.getLogger(SmimeApplet.class.getName()).log(Level.SEVERE, null, ex);
                
            } catch (Throwable ex) { // catch all
                Throwable e = ex.getCause();
                if (e != null) {
                    System.out.println("problema: " + e.getClass().getName() + 
                            " - " + e.getMessage());
                    e.getMessage();
                }
                OutputStream out = new ByteArrayOutputStream();
                PrintWriter prOut = new PrintWriter(out);
                Logger.getLogger(SmimeApplet.class.getName()).log(Level.SEVERE, null, ex);
                prOut.append(ex.getMessage());
                ex.printStackTrace(prOut);
                prOut.flush();
                JSObject.getWindow(this.outerApplet).call("appletStub", new String[]{this._generateJsonResponse(false, out.toString())});
            } finally {
                dc.destroy();
                // todo: return error to browser to cancel mail send
            }

            System.gc();
            //Thread.yield();
            return null;

        }
    }
}
